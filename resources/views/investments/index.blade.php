@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Short Term Investments</h4>
                </div>
                <div class="card-body">
                    <div class="alert alert-info">
                        <span><b>Privileges</b></span>
                        <span>
                            <ul>
                                <li> 50% Interest on Investment amount at Maturity date.</li>
                                <li> Returns after 5 working days(weekdays only).</li>
                                <li> 100% interest + capital withdrawal</li>
                            </ul>
                        </span>
                    </div>

                    <form method="post">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Investment Plan</label>
                                    <select name="amount" class="form-control">
                                        <option>10</option>
                                        <option>20</option>
                                        <option>30</option>
                                        <option>50</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block">Invest Now</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Long Term Investments</h4>
                </div>
                <div class="card-body">
                    <div class="alert alert-info">
                        <span><b>Privileges</b></span>
                        <span>
                            <ul>
                                <li> 50% Interest on Investment amount at Maturity date.</li>
                                <li> Returns after 14 working days(weekdays only).</li>
                                <li> 100% interest + capital withdrawal</li>
                            </ul>
                        </span>
                    </div>

                    <form method="post">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Investment Plan</label>
                                    <select name="amount" class="form-control">
                                        <option>100</option>
                                        <option>300</option>
                                        <option>500</option>
                                        <option>700</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block">Invest Now</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
