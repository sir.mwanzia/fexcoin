@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="row">

        <div class="col-md-4">
            <div class="card" >
                <div class="card-header mb-2" style="background-image: url('/assets/img/bitcoin-3.jpg');background-position: center;" >
                    <h5 class="card-title text-uppercase">Investments Plans</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-category">{{ $investmentsPlansCount }}</h5>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card" >
                <div class="card-header" style="background-image: url('/assets/img/bitcoin-3.jpg');background-position: center;" >
                    <h5 class="card-title text-uppercase">Total Investments</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-category">500</h5>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card" >
                <div class="card-header" style="background-image: url('/assets/img/bitcoin-3.jpg');background-position: center;">
                   <h5 class="card-title text-uppercase">Total Registrations</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-category">{{ $usersCount }}</h5>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <th>
                                <input type="checkbox" class="" value="" checked disabled>
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th class="text-right">
                                created
                            </th>
                            <th class="text-right">
                                updated
                            </th>
                            </thead>
                            <tbody>
                            @foreach($members as $member)
                            <tr>
                                <td>
                                    <input type="checkbox" class="" value="" checked disabled>
                                </td>
                                <td>
                                    {{ $member->name }}
                                </td>
                                <td>
                                    {{ $member->email }}
                                </td>
                                <td class="text-right">
                                    {{ $member->created_at }}
                                </td>
                                <td class="text-right">
                                    {{ $member->updated_at }}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
