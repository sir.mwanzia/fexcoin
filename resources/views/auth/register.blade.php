@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">
                        <div class="justify-content-center">
                            <img src="/images/logo.png" class="mx-auto d-block" width="100" alt="fexcoin logo" />
                        </div>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <label for="name" class="col-form-label text-md-left">Name</label>
                                </div>

                                <div class="col-md-12">
                                    <input id="name" type="text" placeholder="Enter name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <!-- <div class="form-group row">

                                <div class="col-md-12">
                                    <label for="mobile" class="col-form-label text-md-left">Mobile Number</label>
                                </div>

                                <div class="col-md-12">
                                    <input id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" placeholder="Mobile number" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" autofocus>

                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div> -->

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <label for="email" class="col-form-label text-md-left">Email</label>
                                </div>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <label for="password" class="col-form-label text-md-left">Password</label>
                                </div>

                                <div class="col-md-12">
                                    <input id="password" type="password" placeholder="Enter your password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="password-confirm" class="col-form-label text-md-left">Confirm Password</label>
                                </div>
                                <div class="col-md-12">
                                    <input id="password-confirm" placeholder="Re-enter password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form><hr>

                        <div class="d-flex justify-content-center">Have an account?  &nbsp; <a href="/login"> Sign in </a> </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
