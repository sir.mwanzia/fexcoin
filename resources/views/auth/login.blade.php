@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body profile-card">
                        <div class="justify-content-center">
                            <img src="/images/logo.png" class="mx-auto d-block" width="100" alt="fexcoin logo" />
                        </div>
                        <div class="profile-card-inner">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="name" class="col-form-label text-md-left">Email</label>
                                    </div>

                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="name" class="col-form-label text-md-left">Password</label>
                                    </div>

                                    <div class="col-md-12">
                                        <input id="password" type="password" placeholder="Enter your password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class=" col-md-12 row mb-1">
                                        <div class="d-flex justify-content-end">
                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link text-right" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-12 mb-3">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                </div>

                            </form><hr>

                            <div class="d-flex justify-content-center">Don't have account yet?  &nbsp; <a href="/register"> Sign Up </a> </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
