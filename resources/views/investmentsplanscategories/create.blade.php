@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">

                    <div class="alert alert-info">
                        <span><b>Investment Plans Form</b></span>
                    </div>

                    <form action="/investmentsplanscategories/{{ encrypt($Investmentplans->id) }}" enctype="multipart/form-data" method="post" autocomplete="off">

                        @csrf

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Investment Plan Name</label>
                                <select name="plan_id" class="form-control">

                                    <option value="{{ $Investmentplans->id }}" selected>{{ $Investmentplans->title }}</option>

                                </select>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Amount in (<span class="text-danger">$</span>)</label>
                                <input id="amount" type="number" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ old('amount') }}"  autocomplete="amount" autofocus>

                                @error('amount')
                                <span class="invalid-feedback" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Save Information</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-secondary">
                            <th>
                                <input type="checkbox" class="" value="" checked disabled>
                            </th>
                            <th>
                                Investment Plan
                            </th>
                            <th>
                                Investment Parent
                            </th>
                            <th>
                                Action
                            </th>
                            <th class="text-right">
                                Created
                            </th>
                            </thead>
                            <tbody>
                            @foreach($investmentsplanscategories as $investmentplancategory)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="" value="" checked disabled>
                                    </td>
                                    <td class="text-right">
                                        $ {{ $investmentplancategory->amount }}
                                    </td>
                                    <td>
                                        {{ $investmentplancategory->plan_id }}
                                    </td>
                                    <td>
                                        <a href="/investmentsplanscategories/{{ encrypt($investmentplancategory->id) }}" class="btn btn-sm btn-outline-success">update plan</a>
                                    </td>
                                    <td class="text-right">
                                        {{ $investmentplancategory->created_at }}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
