@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">

                    <div class="alert alert-info">
                        <span><b>Investment Plans Form</b></span>
                    </div>

                    <form action="/investmentsplans/{{ encrypt($user->id) }}" enctype="multipart/form-data" method="post">

                        @csrf

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Investment Plan Name</label>
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}"  autocomplete="title" autofocus>

                                @error('title')
                                <span class="invalid-feedback" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Investment Description</label>
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('caption') }}"  autocomplete="description" autofocus>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
				                        <strong>{{ $message }}</strong>
				                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Save Information</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-secondary">
                            <th>
                                <input type="checkbox" class="" value="" checked disabled>
                            </th>
                            <th>
                                Investment
                            </th>
                            <th>
                                Action
                            </th>
                            <th class="text-right">
                                Created
                            </th>
                            </thead>
                            <tbody>
                            @foreach($investments as $investment)
                            <tr>
                                <td>
                                    <input type="checkbox" class="" value="" checked disabled>
                                </td>
                                <td>
                                    {{ $investment->title }}
                                </td>
                                <td>
                                    <a href="/investmentsplanscategories/{{ encrypt($investment->id) }}" class="btn btn-sm btn-outline-success">update plan</a>
                                </td>
                                <td class="text-right">
                                    {{ $investment->created_at }}
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
