@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info alert-with-icon" data-notify="container">
                                <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                                <h5 class="title">Edit Profile</h5>
                                <span data-notify="message">This is a notification with close button and icon and have many lines.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email (disabled)</label>
                                    <input type="Email" class="form-control" disabled="" placeholder="Company" value="{{ $user->email ?? ''}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" placeholder="name" value="{{ $user->name ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-6 pl-1">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" class="form-control" placeholder="mobile" value="{{ $user->profile->mobile ?? '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" class="form-control" placeholder="City" value="{{ $user->profile->city ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-4 px-1">
                                <div class="form-group">
                                    <label>Country</label>
                                    <input type="text" class="form-control" placeholder="Country" value="{{ $user->profile->country ?? '' }}">
                                </div>
                            </div>
                            <div class="col-md-4 pl-1">
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input type="number" class="form-control" placeholder="ZIP Code" value="{{ $user->profile->postal_code ?? '' }}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="image">
                    <img src="../assets/img/bg-1.jpg" alt="...">
                </div>
                <div class="card-body">
                    <div class="author">
                        <a href="#">
                            <img class="avatar border-gray" src="../assets/img/default-avatar.png" alt="...">
                            <h5 class="title">{{ $user->name ?? '' }}</h5>
                        </a>
                        <div class="alert alert-success">
                            <span>Invest in either short term investment or Long term Investment. Current balance <br/><b> {{ $user->balance ?? 'Ksh. 500' }} </b></span>
                        </div>
                        <p class="description">
                            {{ $user->mobile ?? ''}}
                        </p>
                    </div>

                </div>
                <hr>
                <div class="button-container">
                    <div class="row">
                        <div class="col-lg-10 ml-auto mr-auto">
                            <button class ="btn btn-primary btn-block">Topup Wallet</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
