<?php

namespace App\Http\Controllers;

use App\Investmentplan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvestmentsplansController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }

    public function create($user)
    {
        $user = User::findOrFail(Auth::user()->id);

        $investments = Investmentplan::all();


        return view('investmentsplans.create',[
            'user' => $user,
            'investments' => $investments

        ]);
    }

    public function store()
    {

        $data = request()->validate([

            'title' => 'required',
            'description' => 'required',

        ]);

        auth()->user()->Investmentplan()->create([
            'title' => $data['title'],
            'description' => $data['description'],
        ]);

        return redirect('/investmentsplans/' . encrypt(auth()->user()->id));
    }

}
