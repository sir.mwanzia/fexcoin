<?php

namespace App\Http\Controllers;

use App\Investmentplan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class AdminsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }

    public function index($user)
    {
        $user = User::findOrFail(Auth::user()->id);

        $members = User::all();
        $usersCount = User::count();

        $investmentsPlansCount = Investmentplan::count();

//        $encrypted = Crypt::encrypt($user);
//        $decrypted = Crypt::decrypt("");
//        dd($encrypted);

        return view('admins.index',[
            'user' => $user,
            'members' => $members,
            'usersCount' => $usersCount,
            'investmentsPlansCount' => $investmentsPlansCount,

        ]);
    }
}
