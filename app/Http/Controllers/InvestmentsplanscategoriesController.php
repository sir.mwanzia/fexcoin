<?php

namespace App\Http\Controllers;

use App\Investmentplan;
use App\Investmentplancategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvestmentsplanscategoriesController extends Controller
{
    public function create($id)
    {
        $user = User::findOrFail(Auth::user()->id);

        $Investmentplans = Investmentplan::findOrFail(decrypt($id));
        $investmentsplanscategories = Investmentplancategory::all();


        return view('investmentsplanscategories.create',[
            'user' => $user,
            'Investmentplans' => $Investmentplans,
            'investmentsplanscategories' => $investmentsplanscategories

        ]);
    }

    public function store()
    {

        $data = request()->validate([

            'plan_id' => 'required',
            'amount' => 'required',

        ]);

        $plan = Investmentplancategory::create([
            'plan_id' => $data['plan_id'],
            'amount' => $data['amount'],
        ]);

        return redirect('/investmentsplans/' . encrypt(auth()->user()->id));
    }


}
