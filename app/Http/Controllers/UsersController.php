<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:user', 'verified']);
    }

    public function index($user)
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('users.index',[
            'user' => $user,

        ]);
    }
}
