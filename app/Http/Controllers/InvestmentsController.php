<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvestmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:user');
    }

    public function index($user)
    {
        $user = User::findOrFail(Auth::user()->id);

        return view('investments.index',[
            'user' => $user,

        ]);
    }
}
