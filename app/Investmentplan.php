<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investmentplan extends Model
{
    protected $fillable = [
        'title', 'description',
    ];

    protected $guarded = [];

    public function user()
    {
        $this->belongsToMany(User::class);

    }


    public function Investmentplanscategories()
    {

        return $this->hasMany(Investmentsplancategory::class)->orderBy('created_at', 'ASC');
    }
}
