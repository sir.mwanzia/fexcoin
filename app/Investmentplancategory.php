<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investmentplancategory extends Model
{
    protected $fillable = [
        'plan_id', 'amount',
    ];


    public function investmentplancategory()
    {
        $this->belongsTo(Investmentplan::class);
    }
}
