<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['verify' => true]);

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admins/{user_id}', 'AdminsController@index')->name('admins.show');
Route::get('/users/{user_id}', 'UsersController@index')->name('users.show');

//admins
Route::get('/investmentsplans/{user_id}', 'InvestmentsplansController@create');
Route::post('/investmentsplans/{user_id}', 'InvestmentsplansController@store');

Route::get('/investmentsplanscategories/{user_id}', 'InvestmentsplanscategoriesController@create');
Route::post('/investmentsplanscategories/{user_id}', 'InvestmentsplanscategoriesController@store');


//users
Route::get('/investments/{user_id}', 'InvestmentsController@index')->name('investments.show');
